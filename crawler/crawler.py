import numpy as np
import pandas as pd
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

class crawler(object):

    def __init__(self):
        self.profile = webdriver.FirefoxProfile(
            '/home/lukt/.mozilla/firefox/km3jrdtj.default-release/')
        self.browser = webdriver.Firefox(
            executable_path='./geckodriver', firefox_profile  =self.profile)
        self.browser.get('https://www.linkedin.com/jobs')
        self.wait = WebDriverWait(self.browser,20)

    def find_job_offers(self, job_name, job_region):
        clickable_element = expected_conditions.element_to_be_clickable((
            By.XPATH, '//input[contains(@id,"jobs-search-box-keyword-id-ember")]'))
        self.wait.until(clickable_element)
        self.browser.find_element(
            By.XPATH, '//input[contains(@id,"jobs-search-box-keyword-id-ember")]'
            ).send_keys(job_name)
        self.browser.find_element(
            By.XPATH, '//input[contains(@id,"jobs-search-box-keyword-id-ember")]'
            ).send_keys(Keys.RETURN)
        clickable_element = expected_conditions.element_to_be_clickable((
            By.XPATH, '//input[contains(@id,"jobs-search-box-location-id-ember")]'))
        self.wait.until(clickable_element)
        self.browser.find_element(
            By.XPATH, '//input[contains(@id,"jobs-search-box-location-id-ember")]'
            ).clear()
        self.browser.find_element(
            By.XPATH, '//input[contains(@id,"jobs-search-box-location-id-ember")]'
            ).send_keys(job_region)
        self.browser.find_element(
            By.XPATH, '//input[contains(@id,"jobs-search-box-location-id-ember")]'
            ).send_keys(Keys.RETURN)



        
        
        
